{
  inputs = { 
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-22.05";
    julia2nix = {
      url = "gitlab:mireautils/Julia2Nix.jl";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = { self, nixpkgs, julia2nix }: {
    packages.x86_64-linux = let pkgs = nixpkgs.legacyPackages."x86_64-linux";
    in rec {
      # https://github.com/JuliaCN/Julia2Nix.jl/blob/main/flake.nix
      julia = julia2nix.lib.x86_64-linux.julia-wrapped {
        package = julia2nix.packages.x86_64-linux.julia_18-bin;
        enable = {
          GR = true;
          python =
            nixpkgs.legacyPackages.x86_64-linux.python3.buildEnv.override
            {
              extraLibs = with nixpkgs.legacyPackages.x86_64-linux.python3Packages; [xlrd matplotlib tkinter];
              ignoreCollisions = true;
            };
        };
      };
      
      julia-env = julia2nix.lib.x86_64-linux.buildEnv {
        package = julia;
        src = ./julia;
        name = "std-env";
        extraStartup = ''
          pushfirst!(Base.LOAD_PATH, "${./julia}")
          Pkg.activate(joinpath(homedir(), ".julia/environments/v1.8/"))
        '';
      };

      # https://github.com/NixOS/nixpkgs/blob/634141959076a8ab69ca2cca0f266852256d79ee/pkgs/applications/editors/vscode/with-extensions.nix
      vscodium =
        let
          lib = nixpkgs.lib;
          extensions = [{
            publisher = "julialang";
            name = "language-julia";
            version = "1.7.6";
            sha256 = "sha256-AQtKRsBbSbAO1jphs3ytFI4MnbafWSX2RxJtI89Z6dU=";
          }];
          runFlags = lib.concatStringsSep " " (builtins.map (ext:
            let builtExt = pkgs.vscode-utils.extensionFromVscodeMarketplace ext;
            in ''
              --run "ln -sf ${builtExt}/share/vscode/extensions/* ~/.vscode-oss/extensions/"'')
            extensions);
          executableName = "codium";
        in pkgs.runCommand "vscodium-ext" {
          nativeBuildInputs = [ pkgs.makeWrapper ];
          buildInputs = [ pkgs.vscodium ];
          dontPatchElf = true;
          dontStrip = true;
          meta = pkgs.vscodium.meta;
        } ''
          mkdir -p "$out/bin"
          mkdir -p "$out/share/applications"
          mkdir -p "$out/share/pixmaps"
          ln -sT "${pkgs.vscodium}/share/pixmaps/code.png" "$out/share/pixmaps/code.png"
          ln -sT "${pkgs.vscodium}/share/applications/${executableName}.desktop" "$out/share/applications/${executableName}.desktop"
          ln -sT "${pkgs.vscodium}/share/applications/${executableName}-url-handler.desktop" "$out/share/applications/${executableName}-url-handler.desktop"
          makeWrapper "${pkgs.vscodium}/bin/${executableName}" "$out/bin/${executableName}" --run "mkdir -p ~/.vscode-oss/extensions" ${runFlags}
        '';

      bundle = pkgs.buildEnv {
        name = "bundle";
        paths = [ julia-env vscodium ];
      };
    };
  };
}
